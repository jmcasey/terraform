resource "digitalocean_vpc" "bastion" {
  name     = var.digital_ocean_vpc
  region = "${var.digital_ocean_region}"
  ip_range = var.digital_ocean_vpc_cidr
}