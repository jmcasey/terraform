variable "digital_ocean_token" {
  description = "Authentication Token For Digital Ocean"
}
variable "digital_ocean_region" {
  description = "The Digital Ocean Region Slug: https://docs.digitalocean.com/products/platform/availability-matrix/"
  default     = "sfo3"
}
variable "digital_ocean_droplet_slug" {
  description = "The Default Digital Ocean Droplet Slug: https://slugs.do-api.dev/"
  default     = "s-1vcpu-1gb"
}
variable "digital_ocean_image" {
  description = "Digital Ocean Disto Image: https://slugs.do-api.dev/"
  default     = "debian-11-x64"
}
variable "digital_ocean_vpc_cidr" {
  description = "Digital Ocean VPC CIDR"
}
variable "digital_ocean_vpc" {
  description = "Digital Ocean VPC"
  default     = "bastion-vpc"
}
variable "private_key_path" {
  description = "ssh private key file path"
}
variable "digital_ocean_ssh_key" {
  description = "public fingerprint"
}
