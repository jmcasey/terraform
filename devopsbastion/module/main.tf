resource "digitalocean_droplet" "bastion" {
  image    = var.digital_ocean_image
  name     = "bastion"
  region   = var.digital_ocean_region
  size     = var.digital_ocean_droplet_slug
  vpc_uuid = digitalocean_vpc.bastion.id
  ssh_keys = var.digital_ocean_ssh_key

  connection {
      host = self.ipv4_address
      user = "root"
      type = "ssh"
      private_key = file(var.private_key_path)
      timeout = "2m"
  }

  provisioner "remote-exec" {
    inline = [
      "curl https://gitlab.com/jmcasey/scripts/-/raw/main/install-docker.sh | sh",
      "curl https://gitlab.com/jmcasey/scripts/-/raw/main/install-terraform.sh | sh",
      "curl https://gitlab.com/jmcasey/scripts/-/raw/main/install-portainer.sh | sh",
    ]
  }
}

