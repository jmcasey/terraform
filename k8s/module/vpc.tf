resource "digitalocean_vpc" "k8s" {
  name     = "k8s-vpc"
  region = "${var.digital_ocean_region}"
  ip_range = var.digital_ocean_vpc_cidr
}