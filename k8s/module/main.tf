resource "digitalocean_kubernetes_cluster" "k8s" {
  depends_on = [time_sleep.wait_for_k8s]
 
  name    = var.k8s_cluster_name
  region  = var.digital_ocean_region
  version = var.digital_ocean_k8s_version

  # network
  vpc_uuid = digitalocean_vpc.k8s.id

  node_pool {
    name       = "nodepool"
    size       = var.digital_ocean_droplet_slug
    node_count = var.k8s_node_count
  }
}


# Digital Ocean requires some time after deleting the cluster before the
# the VPC can be deleted. Testing showed it to be approximately 5 minutes.
resource "time_sleep" "wait_for_k8s" {
  depends_on = [digitalocean_vpc.k8s]
  destroy_duration = "300s"
}