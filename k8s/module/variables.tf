variable "digital_ocean_token" {
  description = "Authentication Token For Digital Ocean"
}
variable "digital_ocean_region" {
  description = "The Default Digital Ocean Region Slug: https://docs.digitalocean.com/products/platform/availability-matrix/"
  default     = "nyc3"
}
variable "digital_ocean_k8s_version" {
  description = "Grab the latest version slug from `doctl kubernetes options versions"
}
variable "digital_ocean_droplet_slug" {
  description = "The Default Digital Ocean Droplet Slug: https://slugs.do-api.dev/"
  default     = "s-2vcpu-2gb"
}
variable "digital_ocean_image" {
  description = "Default Image for Ubuntu LTS"
  default     = "dunno"
}
variable "digital_ocean_vpc_cidr" {
  description = "Digital Ocean VPC CIDR"
  default     = "10.10.0.0/16"
}
variable "k8s_cluster_name" {
  description = "Digital Ocean Name for Cluster"
  default     = "fancyname"
}
variable "k8s_node_count" {
  description = "Number of nodes to create and add to k8s cluster"
  default     = "3"
}