# terraform

Repo for Terraform projects.

~~Definitely don't use these.~~ Main branch promoted to "unstable".

## projects

Usage:

All currently rely on a top-level instansiation module. TODO: Refactor to allow deployment variable input from the CLI, as well as a more standard secrets/deployment management model.

### kasm

[Self-hosted remote workspaces](https://github.com/kasmtech/KasmVNC?__hstc=&__hssc=&hsCtaTracking=5647b03d-8cb1-41f9-af4d-4bf4b1692a5b%7C3382cb29-bf73-4c56-b7a6-d8e975e39fce)

### devopsbastion
One click DO droplet deployment and terraform/docker/portainer installation.

# k8s
One click kubernetes cluster initialisation and deployment.

TODO:

- Merge with `code-server` IDE.
- Create docker image.

# Terraform Initialisation and Deployment

`terraform init && terraform plan && terraform apply`

