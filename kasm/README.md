# DigitalOcean (Single-Server)
This project will deploy Kasm Workspaces in a single-server deployment on DigitalOcean.

# Terraform Initialisation and Deployment

`terraform init && terraform plan && terraform apply`